<?php
echo $problem->text;
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'form',
	'enableAjaxValidation'=>false,
));
    echo $form->errorSummary($answers);
    echo $form->radioButtonList($answers,'problemId', $list);
    echo $form->hiddenField($problem,'mistake');
    echo $form->hiddenField($problem,'problemId');
    echo CHtml::hiddenField('testProblemId',$testProblemId);
    echo $form->hiddenField($problem,'testId');
    echo CHtml::hiddenField('testId',$testId);
    echo CHtml::hiddenField('additionalLeftTasks',$additionalLeftTasks);

    ?>
    <br/>
    <?php
    echo CHtml::ajaxSubmitButton('Next', array('test/index'),
            array(
                'type' => 'POST',
                'update' => '#task',
                            ),
            array(
                'id' => 'nextButton'.$problem->testProblemId,
                'type' => 'submit',
            ));
$this->endWidget();

?>